package com.zuitt.example;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        System.out.println("Input an integer whose factorial will be computed");

        Scanner in = new Scanner(System.in);


        int answer = 1;
        int counter = 1;

        try{
            counter = in.nextInt();

            if (counter <= 0){
                System.out.println("The number " + counter + " in not possible to be factorial. Only positive numbers are accepted");
            }
            else {
                for(int i = 1; i <= counter; i++){
                    answer = i * answer;
                }
                System.out.println("The factorial of " + counter + " is " + answer);
            }

        }
        catch (Exception ex){
            System.out.println("Invalid Input");
            ex.printStackTrace();
        }
    }
}
